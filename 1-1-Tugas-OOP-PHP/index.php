<?php

trait Hewan
{
 public $nama, $darah = 50, $jumlahKaki, $keahlian;

 public function atraksi()
 {
  return "{$this->nama} sedang {$this->keahlian}";
 }
}

trait Fight
{
 public $attackPower, $defencePower;

 public function serang($lawan)
 {
  return "{$this->nama} sedang menyerang {$lawan->nama}";
 }

 public function diserang($lawan)
 {
  $this->darah = $this->darah - ($lawan->attackPower / $this->defencePower);
  return "{$this->nama} sedang diserang ";
 }
}

class Harimau
{

 use Hewan, Fight;

 public function __construct($nama)
 {
  $this->nama = $nama;
  $this->jumlahKaki = 4;
  $this->keahlian = "lari cepat";
  $this->attackPower = 7;
  $this->defencePower = 8;
 }

 public function getInfoHewan()
 {
  echo "Nama : <b> $this->nama </b>  <br>";
  echo "darah : <b> $this->darah </b><br>";
  echo "jumlah kaki : <b> $this->jumlahKaki </b><br>";
  echo "keahlian : <b> $this->keahlian </b><br>";
  echo "Attack Power : <b> $this->attackPower </b><br>";
  echo "Defence Power : <b> $this->defencePower </b><br>";
  echo "Jenis hewan : <b>" . get_class($this) . "</b>";
 }
}

class Elang
{
 use Hewan, Fight;
 public function __construct($nama)
 {
  $this->nama = $nama;
  $this->jumlahKaki = 2;
  $this->keahlian = "terbang tinggi";
  $this->attackPower = 10;
  $this->defencePower = 5;
 }
 public function getInfoHewan()
 {
  echo "Nama : <b> $this->nama </b>  <br>";
  echo "darah : <b> $this->darah </b><br>";
  echo "jumlah kaki : <b> $this->jumlahKaki </b><br>";
  echo "keahlian : <b> $this->keahlian </b><br>";
  echo "Attack Power : <b> $this->attackPower </b><br>";
  echo "Defence Power : <b> $this->defencePower </b><br>";
  echo "Jenis hewan : <b>" . get_class($this) . "</b>";
 }
}


$harimau_1 = new Harimau('harimau_1');
$harimau_2 = new Harimau('harimau_2');
$elang_3 = new Elang('elang_3');
$elang_4 = new Elang('elang_4');

echo "<h2>Fight 1</h2><br>";

echo $elang_3->atraksi() . "<br>";
echo $harimau_1->serang($elang_3) . "<br>";
echo $elang_3->diSerang($harimau_1) . "<br><hr>";
echo $elang_3->getInfoHewan() . "<br><br>";

echo "<h2>Fight 2</h2><br>";

echo $harimau_2->atraksi() . "<br>";
echo $elang_4->serang($harimau_2) . "<br>";
echo $harimau_2->diSerang($elang_4) . "<br><br><hr>";
echo $harimau_2->getInfoHewan() . "<br><br>";
