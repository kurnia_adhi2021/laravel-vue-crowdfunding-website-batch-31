// SOAL 1

// rumus luas persegi panjang

const luas = (panjang, lebar) => {
  return panjang * lebar
}

// rumus keliling persegi panjang

const keliling = (panjang, lebar) => {
  return 2 * (panjang + lebar)
}

console.log(luas(7, 5))
console.log(keliling(7, 5))

// SOAL 2

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function () {
      console.log(firstName + ' ' + lastName)
    },
  }
}

newFunction('William', 'Imoh').fullName()

// SOAL 3

const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
}

const { firstName, lastName, address, hobby } = newObject

console.log(firstName, lastName, address, hobby)

// SOAL 4

const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']

const combined = [...west, ...east]

console.log(combined)

// SOAL 5

const planet = 'earth'
const view = 'glass'
var before =
  'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

var newSentence = `Lorem ${view} dolor sit amet , consectetur adipiscing elit, ${planet}`

console.log(newSentence)
