// Soal 1

var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek']

for (let index = 0; index < daftarHewan.length; index++) {
  console.log(daftarHewan.sort()[index])
}

// Soal 2

function introduce() {
  return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`
}

var data = {
  name: 'John',
  age: 30,
  address: 'Jalan Pelesiran',
  hobby: 'Gaming',
}

var perkenalan = introduce(data)

console.log(perkenalan)

// Soal 3

function hitung_huruf_vokal(str) {
  const count = str.match(/[aeiou]/gi).length
  return count
}

let hitung_1 = hitung_huruf_vokal('Muhammad')
let hitung_2 = hitung_huruf_vokal('Iqbal')
console.log(hitung_1, hitung_2)

// soal 4

function hitung(x) {
  return x - 2 + x
}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))
